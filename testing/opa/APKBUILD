# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=opa
pkgver=0.56.0
pkgrel=1
pkgdesc="Open source, general-purpose policy engine"
url="https://www.openpolicyagent.org/"
# only platforms with full +wasm support, as upstream does
#arch="aarch64 x86_64"
arch=""
license="Apache-2.0"
makedepends="go wasmtime-dev"
checkdepends="tzdata"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/open-policy-agent/opa/archive/refs/tags/v$pkgver.tar.gz"

export CGO_ENABLED=1
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	# report.ExternalServiceURL is left blank to disable telemetry
	# - "opa version -c" errors out
	# - "opa run" fails the version check process silently in the background
	# this is intended :)
	local goldflags="
	-X github.com/open-policy-agent/opa/version.Version=$pkgver
	-X github.com/open-policy-agent/opa/version.Hostname=AlpineLinux
	-X github.com/open-policy-agent/opa/version.Vcs=0000000000000000000000000000000000000000
	-X github.com/open-policy-agent/opa/version.Timestamp=$(date -u "+%Y-%m-%dT%H:%M:%SZ" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	-X github.com/open-policy-agent/opa/internal/report.ExternalServiceURL=
	"

	export CGO_CFLAGS="$CFLAGS"
	export CGO_LDFLAGS="$LDFLAGS -L/usr/lib -lwasmtime"

	go build -v -ldflags "$goldflags" -tags opa_wasm

	mkdir -p man/
	go run build/generate-man/generate.go man/

	for shell in bash fish zsh; do
		./opa completion $shell > opa.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 opa -t "$pkgdir"/usr/bin/

	install -Dm644 man/*.1 -t "$pkgdir"/usr/share/man/man1/

	install -Dm644 opa.bash \
		"$pkgdir"/usr/share/bash-completion/completions/opa
	install -Dm644 opa.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/opa.fish
	install -Dm644 opa.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_opa
}

sha512sums="
dca6f77ca1e015275404f25801f71370e72d515e9d4861cc911f1172b5fdb2d50083114655275e5c87bbb82956f6e6eace1f85a29abd37081a8cecebd047af0b  opa-0.56.0.tar.gz
"
