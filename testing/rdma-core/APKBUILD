# Maintainer:
pkgname=rdma-core
pkgver=47.0
pkgrel=1
pkgdesc="RDMA core userspace libraries and daemons"
url="https://github.com/linux-rdma/rdma-core"
license="GPL-2.0-only OR BSD-2-Clause"
arch="all !s390x !riscv64" # rv64 textrels, s390x ftbfs
options="!check" # tests are for installation
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	cmake
	eudev-dev
	libdrm-dev
	libnl3-dev
	py3-docutils
	samurai
	"
subpackages="$pkgname-libs $pkgname-dev $pkgname-doc $pkgname-openrc"
source="https://github.com/linux-rdma/rdma-core/releases/download/v$pkgver/rdma-core-$pkgver.tar.gz"

build() {
	CFLAGS="$CFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	rm -rf "$pkgdir"/usr/lib/systemd
}

sha512sums="
6de17576f27204ed3e3a4a386ecd5ca20bbef4f23b16cdeacb362457db652ee790e1b1aab46499393908994f40c79e8d184ceb9995b29bc5c9ad22170d599f2e  rdma-core-47.0.tar.gz
"
