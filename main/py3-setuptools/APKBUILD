# Contributor: Andrew Manison <amanison@anselsystems.com>
# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Peter Shkenev <santurysim@gmail.com>
pkgname=py3-setuptools
pkgver=68.2.0
pkgrel=0
pkgdesc="Collection of enhancements to the Python3 distutils"
options="!check" # Tests require packages out of main/
url="https://pypi.org/project/setuptools"
arch="noarch"
license="MIT"
# everything is vendored
depends="
	py3-packaging
	python3
	"
# depends="
# 	py3-appdirs
# 	py3-more-itertools
# 	py3-ordered-set
# 	py3-packaging
# 	python3
# 	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver-2.tar.gz::https://pypi.io/packages/source/s/setuptools/setuptools-$pkgver.tar.gz"
builddir="$srcdir"/setuptools-$pkgver

provides="py-setuptools=$pkgver-r$pkgrel" # Backwards compatibility
replaces="py-setuptools" # Backwards compatiblity

# py3-setuptools needs itself to build, bootstrapped with a lower version
# in main/py3-setuptools-stage0
provides="$provides py3-setuptools-bootstrap"
provider_priority=100 # highest

export SETUPTOOLS_INSTALL_WINDOWS_SPECIFIC_FILES=0

prepare() {
	default_prepare

	# Unbundle
	# rm -rf pkg_resources/extern pkg_resources/_vendor \
	# 	setuptools/extern setuptools/_vendor

	# Upstream devendoring logic is badly broken, see:
	# https://bugs.archlinux.org/task/58670
	# https://github.com/pypa/pip/issues/5429
	# https://github.com/pypa/setuptools/issues/1383
	# The simplest fix is to simply rewrite import paths to use the canonical
	# location in the first place
	# for _module in setuptools pkg_resources '' ; do
	# 	find . -name \*.py -exec sed -i \
	# 		-e 's/from '$_module.extern' import/import/' \
	# 		-e 's/from '$_module.extern'./from /' \
	# 		-e 's/import '$_module.extern'./import /' \
	# 		-e "s/__import__('$_module.extern./__import__('/" \
	# 		{} +
	# done

	# Fix post-release tag
	sed -e '/tag_build = .post/d' \
		-e '/tag_date = 1/d' \
		-i setup.cfg
}

build() {
	python3 setup.py build
}

package() {
	# Otherwise it complains that build/scripts-3.10 cannot be found
	# no other changes noted
	mkdir -p build/scripts-3.11

	python3 setup.py install --root="$pkgdir" --skip-build
}

sha512sums="
b9a586cfb97bfadc7f2874380270a946c240ddc8c2ea508b37887ed34bb4273a56411147eb11631252950594d72e36e7c9ff0378fea6f78ed7f3946dbe9dd4a0  py3-setuptools-68.2.0-2.tar.gz
"
