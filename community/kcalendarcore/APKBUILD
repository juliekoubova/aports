# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kcalendarcore
pkgver=5.109.0
pkgrel=0
pkgdesc="The KDE calendar access library"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends_dev="
	libical-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
_repo_url="https://invent.kde.org/frameworks/kcalendarcore.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcalendarcore-$pkgver.tar.xz"
options="!check" # RecursOn-ConnectDaily(2|3|6) make the builders stuck

replaces="kcalcore"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# testrecurtodo, testreadrecurrenceid, testicaltimezones, testmemorycalendar and testtimesininterval are broken
	ctest --test-dir build --output-on-failure -E "test(recurtodo|readrecurrenceid|icaltimezones|memorycalendar|timesininterval)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fe1c60ca3d002db2e1b83f2cfc8a15b875cf365d81b471c21c2f094baf313ee40f3bf3dc29d90b1dd760ac721f58e6db51b45276f93ef3c983148d17f996f927  kcalendarcore-5.109.0.tar.xz
"
