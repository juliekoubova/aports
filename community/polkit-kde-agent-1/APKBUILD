# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=polkit-kde-agent-1
pkgver=5.27.7
pkgrel=1
pkgdesc="Daemon providing a polkit authentication UI for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="polkit-elogind"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/polkit-kde-agent-1.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/polkit-kde-agent-1-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
f4474b6551a310fe2b2e0777bdf64cd596e9994dbf3a58c7720abec3960e63a2de2d36e599d6d06a34b27280aea78eecbeb24a28c028f4f27668bdf5c22ed72c  polkit-kde-agent-1-5.27.7.tar.xz
"
