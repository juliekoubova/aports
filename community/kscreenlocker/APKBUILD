# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kscreenlocker
pkgver=5.27.7
pkgrel=1
pkgdesc="Library and components for secure lock screen architecture"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends="
	elogind
	linux-pam
	"
depends_dev="
	elogind-dev
	kcmutils-dev
	kcrash-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kidletime-dev
	knotifications-dev
	libkscreen-dev
	ktextwidgets-dev
	kwayland-dev
	kwindowsystem-dev
	kxmlgui-dev
	layer-shell-qt-dev
	libseccomp-dev
	linux-pam-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	solid-dev
	xcb-util-keysyms-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kscreenlocker.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kscreenlocker-$pkgver.tar.xz
	kde.pam
	kde-np.pam
	0001-Prevent-finishing-greeter-by-unhandled-signals.patch
	"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires running loginctl

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	install -D -m644 "$srcdir"/kde.pam "$pkgdir"/etc/pam.d/kde
	install -m644 "$srcdir"/kde-np.pam "$pkgdir"/etc/pam.d/kde-np
}

sha512sums="
7005e4fac424f819ed875c4c439d2f4e7c26f8ab4afc29dceaca0a01ae3794ef414f8d265fc0d750b515677aa7141a3db8cd9b2495108030407c269911df3ea2  kscreenlocker-5.27.7.tar.xz
56e87d02d75c4a8cc4ed183faed416fb4972e7f223b8759959c0f5da32e11e657907a1df279d62a44a6a174f5aca8b2ac66a5f3325c5deb92011bcf71eed74c3  kde.pam
565265485dd7466b77966d75a56766216b8bcc187c95a997e531e9481cf50ddbe576071eb0e334421202bcab19aa6de6b93e042447ca4797a24bf97e1d053ffd  kde-np.pam
a59f38a86e94744746a27e3e0de7742290cff8bfd63d7ba964c93b5ae17a3f709916f9f494fa7ba9e971dd4ca8a769b9eaeb04f785e71b4f1f3a27397273b3a7  0001-Prevent-finishing-greeter-by-unhandled-signals.patch
"
