# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=systemsettings
pkgver=5.27.7
pkgrel=1
pkgdesc="Plasma system manager for hardware, software, and workspaces"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kactivities-stats-dev
	kcmutils-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kirigami2-dev
	kitemviews-dev
	kpackage-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/systemsettings.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/systemsettings-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-zsh-completion $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ab8f18d99942b3e412dd5a4217b2ac5816662fde100a318291ff6451dd58691d458645a9c7697bab75f8a263dd80acda273afcfed05ab42db4708e8b18f2af37  systemsettings-5.27.7.tar.xz
"
