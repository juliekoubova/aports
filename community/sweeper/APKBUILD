# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=sweeper
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.sweeper"
pkgdesc="System cleaner to help clean unwanted traces the user leaves on the system"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	kactivities-stats-dev
	kbookmarks-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/sweeper.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/sweeper-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3fd7aa43daf76706a635e568750ab71e11561375e64805e55bb16a866c38a189a832b6a68e89b71b140d1e6ea146ca77bb59a7a640d8a296388539db07dfef53  sweeper-23.08.0.tar.xz
"
