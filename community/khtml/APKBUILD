# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=khtml
pkgver=5.109.0
pkgrel=0
pkgdesc="The KDE HTML library, ancestor of WebKit"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND LGPL-2.1-only"
depends_dev="
	giflib-dev
	karchive-dev
	kcodecs-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjs-dev
	knotifications-dev
	kparts-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libjpeg-turbo-dev
	perl-dev
	qt5-qtbase-dev
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	gperf
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/khtml.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/khtml-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6f96e04a5f8d5d4f48e6ab2f7c79d82e91e5bd658620528c9d00c60bee8a1270f9a5f0366988e320fb9d184475227e27c107ee93b05a8798b9354eb215e176fd  khtml-5.109.0.tar.xz
"
