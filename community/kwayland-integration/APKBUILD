# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kwayland-integration
pkgver=5.27.7
pkgrel=1
pkgdesc="KWayland integration"
url="https://kde.org/plasma-desktop/"
arch="all !armhf" # armhf blocked by extra-cmake-modules
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="kglobalaccel"
makedepends="
	extra-cmake-modules
	kguiaddons-dev
	kidletime-dev
	kwayland-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kwayland-integration.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-integration-$pkgver.tar.xz"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c2dee5bc7d101582e156ebf4587f0059a109ea0e201cf12b3736e9daa41677fc57780afa6631b0f407c062c9aa6672f46fa657c28ca6617d032fc88b56cebbc9  kwayland-integration-5.27.7.tar.xz
"
