# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ktnef
pkgver=23.08.0
pkgrel=0
pkgdesc="API for handling TNEF data"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kcalendarcore-dev
	kcalutils-dev
	kcontacts-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/pim/ktnef.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktnef-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
8c43bead30b80b25207b159ca524f3ce4cacf8d3b9157cb4dfabfb494b05352c62281da7f9ee3e675ad513669b3d49030986a797680ef1ce9e640528e04ac8c9  ktnef-23.08.0.tar.xz
"
