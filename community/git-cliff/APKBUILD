# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=git-cliff
pkgver=1.3.0
pkgrel=0
pkgdesc="A highly customizable changelog generator"
url="https://github.com/orhun/git-cliff"
# s390x, ppc64le, riscv64: blocked by ring crate
arch="all !s390x !ppc64le !riscv64"
license="GPL-3.0-or-later"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/git-cliff/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
	mkdir -p man
	OUT_DIR=man/ "./target/release/$pkgname-mangen"
	mkdir -p completions
	OUT_DIR=completions/ "./target/release/$pkgname-completions"
}

check() {
	cargo test --frozen -- --skip "git_log"
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 "man/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
	install -Dm 644 "completions/$pkgname.bash" "$pkgdir/usr/share/bash-completion/completions/$pkgname"
	install -Dm 644 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d"
	install -Dm 644 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions"
}

sha512sums="
b0b6fc06da9963a4c1a736c143eaa8624c1f2264d142170969ea9df6d6ab81824401e5634548cb1978ab76cf0ae8da40a75bb55248ad9c5382c7655b77c775c6  git-cliff-1.3.0.tar.gz
"
