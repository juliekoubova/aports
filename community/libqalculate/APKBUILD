# Contributor: Sodface <sod@sodface.com>
# Maintainer: lonjil <alpine@lonjil.xyz>
pkgname=libqalculate
# the pkgver must be kept in sync with: qalculate-gtk, qalculate-qt
pkgver=4.8.0
pkgrel=0
pkgdesc="Library implementing a multi-purpose desktop calculator"
url="https://qalculate.github.io/"
license="GPL-2.0-or-later"
arch="all"
makedepends="
	curl-dev
	diffutils
	gettext-dev
	gmp-dev
	gnu-libiconv-dev
	icu-dev
	intltool
	libxml2-dev
	mpfr-dev
	readline-dev
	"
subpackages="
	$pkgname-dev
	$pkgname-lang
	qalc
	qalc-doc
	"
source="https://github.com/Qalculate/libqalculate/releases/download/v$pkgver/libqalculate-$pkgver.tar.gz
	libqalculate.pc.in.patch
	"
options="!check" # no test suite

build() {
	# Compile with LTO to reduce binary size.
	export CXXFLAGS="$CXXFLAGS -flto=auto"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

qalc() {
	pkgdesc="Powerful and easy to use command line calculator"
	amove usr/bin/qalc
}

doc() {
	default_doc
	pkgdesc="Powerful and easy to use command line calculator (documentation)"
}

sha512sums="
db41bf883520d72f5d7c80b78492aba5367e74a6c5c452a420d6ecc79ae8f1dfa208c13bfff96ea699d31189cc49c264cf1b543946e4ec39b9df97a386c215ea  libqalculate-4.8.0.tar.gz
a44bb2d18b6ac9306587cfaeec21955aa526faa4abe5f639ab5912227331eb052ec900626619ff939c9f93fb0e7a25d0af134b645bcde5bc694f78af8f4b0af5  libqalculate.pc.in.patch
"
