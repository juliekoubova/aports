# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-pytest-httpx
pkgver=0.24.0
pkgrel=0
pkgdesc="send responses to httpx"
url="https://colin-b.github.io/pytest_httpx/"
arch="all !armhf !ppc64le" #limited by py3-httpx
license="MIT"
depends="py3-httpx py3-pytest"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest-asyncio"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Colin-b/pytest_httpx/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/pytest_httpx-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	#deselected tests fail on [b''] not equal to []
	.testenv/bin/python3 -m pytest \
		--deselect tests/test_httpx_async.py::test_default_response_streaming \
		--deselect tests/test_httpx_sync.py::test_default_response_streaming
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
ca1f470e8f23f1b6919ca6ad7d28438f8d0802e91696434d72db54293dade35d941b18a42b4f9dbba8d2bf5866075bed50f1df70bf7f82f10a6c9f4a6e6399e6  py3-pytest-httpx-0.24.0.tar.gz
"
