# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=networkmanager-qt
pkgver=5.109.0
pkgrel=0
pkgdesc="Qt wrapper for NetworkManager API"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="networkmanager"
depends_dev="networkmanager-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
_repo_url="https://invent.kde.org/frameworks/networkmanager-qt.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/networkmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# The excluded tests currently fail
	ctest --test-dir build --output-on-failure -E '(manager|settings|activeconnection)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
14b13309b5b91d089dd4352e209da1e89a6a7d0e4301e77cf8ff6e36dd204b998b131c294efd35eb6648d5f8340df4ece06ac28b5a0eb8520c2eec29601ec476  networkmanager-qt-5.109.0.tar.xz
"
