# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ffmpegthumbs
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# not useful on s390x
arch="all !s390x !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="FFmpeg-based thumbnail creator for video files"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ffmpeg-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	taglib-dev
	"
_repo_url="https://invent.kde.org/multimedia/ffmpegthumbs.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ffmpegthumbs-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3577e507a49d773b25357fc3a24771c1439eb7c151750258b6ae67b89609cd526a1fe34d15f5fb1c3f5fc56fb59384f2423daefc25eb5ccd375af4d3f32d9c86  ffmpegthumbs-23.08.0.tar.xz
"
